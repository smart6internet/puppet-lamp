# README #

Scripts para automatizar a instalação de um server LAMP.

# Parte I - Usando os Scripts Puppet

Siga os comandos abaixo em um CentOS 7 padrão, como root:

```console
# yum update
# rpm -ivh http://yum.puppetlabs.com/puppetlabs-release-el-7.noarch.rpm
# yum install puppet
# yum install git
# cd /etc/puppet
# puppet resource package puppet ensure=latest
# git init
# git remote add bitbucket https://mbbarcelos@bitbucket.org/smart6internet/puppet-lamp.git
# git pull bitbucket master
# puppet apply --verbose manifests/install_lamp.pp
# mysql_secure_installation
```

>**Comentário:**
>Não esqueça de rodar o script final de mysql_secure_installation para configurar a senha de root do MySQL e retirar contas padrão

Basta acessar o phpMyAdmin através de http://<server_address>/phpmyadmin/.

Não esqueça de alterar as regras de política de segurança da Cloud Smart6 para permitir o acesso http e https ao seu novo server.