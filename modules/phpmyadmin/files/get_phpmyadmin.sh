#!/bin/bash
echo 'WGET PhpMyAdmin'
wget https://files.phpmyadmin.net/phpMyAdmin/4.4.15.7/phpMyAdmin-4.4.15.7-english.tar.gz -O phpmyadmin.tgz
echo 'TAR Extract'
tar -xzvf ./phpmyadmin.tgz
echo 'Delete previous directory'
rm -Rf ./phpmyadmin/
echo 'Change name for phpmyadmin only'
mv phpMyAdmin-* phpmyadmin
echo 'Change owner from root=>apache'
chown -R apache:apache phpmyadmin
echo 'File cleaning'
rm -f ./phpmyadmin.tgz
