class phpmyadmin {

#  package { 'wget':
#    ensure => installed,
#  }

  file { "/tmp/get_phpmyadmin.sh":
    ensure => present,
    source => "puppet:///modules/phpmyadmin/get_phpmyadmin.sh",
    owner => "root",
    group => "root",
    mode => 0744,
    require => Package["wget"],
  }

  exec { "phpMyAdmin" :
    command => "/tmp/get_phpmyadmin.sh",
    cwd => "/var/www/html",
    user => "root",
    require => File["/tmp/get_phpmyadmin.sh"],
    notify => Service["httpd"],
  }

}
