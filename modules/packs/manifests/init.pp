class packs {

#  $phpers = 'php'
  $phpers = [ "php", "php-mcrypt", "php-pdo", "php-mysql", "php-gd", "php-xml", "php-mbstring", "php-cli" ]

  package { $phpers : 
    ensure => installed,
  }

  package { 'wget' :
    ensure => installed,
  }

}
